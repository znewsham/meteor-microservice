import { Meteor } from "meteor/meteor";
import { DDPCommon } from "meteor/ddp-common";
import { Connection } from "meteor/ddp-client/common/livedata_connection.js";
import {
  hasOwn
} from "meteor/ddp-common/utils.js";


export default class PassthroughConnection extends Connection {
  static DEFAULT_MESSAGES_TO_IGNORE = ["pong"];

  static DEFAULT_SIMPLE_MESSAGES = ["added", "changed", "removed", "nosub", "ready"];

  static DEFAULT_LOGIN_TOKEN_TIMEOUT = 90 * 24 * 60 * 60 * 1000;

  constructor(service, connection, ddpAddress, options = {}) {
    super(ddpAddress, options);
    this.downStreamConnection = connection;
    this.subscriptions = {};
    this.methods = {};
    this.timeouts = {};
    this.documents = {};
    this.service = service;
    this.requireLogin = options.requireLogin || false;
    this.loginMethod = options.loginMethod || (() => {});
    this.hasBeenConnected = false;
    this.loginTokenTimeout = PassthroughConnection.DEFAULT_LOGIN_TOKEN_TIMEOUT;
    this.messagesToIgnore = PassthroughConnection.DEFAULT_MESSAGES_TO_IGNORE;
    this.simpleMessages = PassthroughConnection.DEFAULT_SIMPLE_MESSAGES;
    this._stream.on("disconnect", this.onDisconnect.bind(this));
    this.retryCount = 0;
  }

  onDisconnect() {
    if (this.hasBeenConnected) {
      this.retryCount = 0;
      this.hasBeenConnected = false;
      const downstream = this.getDownstream();
      if (downstream) {
        Object.keys(this.documents).forEach((doc) => {
          const [collection, id] = doc.split("::");
          downstream.send({ msg: "removed", collection, id });
        });
        this.documents = {};
        Object.keys(this.subscriptions).forEach((subscriptionId) => {
          downstream.send({ msg: "nosub", id: subscriptionId });
        });
      }
      this.service.tryToClose(this.downStreamConnection.id, true);
    }
    this.retryCount++;
    if (this.retryCount > 3) {
      this.service.tryToClose(this.downStreamConnection.id, true, true);
    }
  }

  onTimeout(type, upstreamId, downstreamId) {
    if (type === "publish") {
      const downstream = this.getDownstream();
      if (downstream) {
        downstream.send({
          msg: "nosub",
          id: downstreamId,
          error: {
            isClientSafe: true,
            error: 504,
            reason: "Request timed out",
            message: "Request timed out [504]",
            errorType: Meteor.Error
          }
        });
      }
      delete this.timeouts[upstreamId];
      delete this.subscriptions[upstreamId];
    }
    else if (type === "method") {
      this.methods[upstreamId].throw(new Meteor.Error(504, "Request timed out"));
      delete this.methods[upstreamId];
      delete this.timeouts[upstreamId];
    }
  }

  /**
   * @memberOf Meteor
   * @importFromPackage meteor
   * @alias Meteor.subscribe
   * @summary Subscribe to a record set.  Returns a handle that provides
   * `stop()` and `ready()` methods.
   * @locus Client
   * @param {String} name Name of the subscription.  Matches the name of the
   * server's `publish()` call.
   * @param {EJSONable} [arg1,arg2...] Optional arguments passed to publisher
   * function on server.
   * @param {Function|Object} [callbacks] Optional. May include `onStop`
   * and `onReady` callbacks. If there is an error, it is passed as an
   * argument to `onStop`. If a function is passed instead of an object, it
   * is interpreted as an `onReady` callback.
   */
  subscribe(id, name, ...args) {
    const self = this;
    const params = args;
    let callbacks = Object.create(null);
    if (params.length) {
      const lastParam = params[params.length - 1];
      if (typeof lastParam === "function") {
        callbacks.onReady = params.pop();
      }
      else if (lastParam && [
        lastParam.onReady,
        // XXX COMPAT WITH 1.0.3.1 onError used to exist, but now we use
        // onStop with an error callback instead.
        lastParam.onError,
        lastParam.onStop
      ].some(f => typeof f === "function")) {
        callbacks = params.pop();
      }
    }

    // Is there an existing sub with the same name and param, run in an
    // invalidated Computation? This will happen if we are rerunning an
    // existing computation.
    //
    // For example, consider a rerun of:
    //
    //     Tracker.autorun(function () {
    //       Meteor.subscribe("foo", Session.get("foo"));
    //       Meteor.subscribe("bar", Session.get("bar"));
    //     });
    //
    // If "foo" has changed but "bar" has not, we will match the "bar"
    // subcribe to an existing inactive subscription in order to not
    // unsub and resub the subscription unnecessarily.
    //
    // We only look for one such sub; if there are N apparently-identical subs
    // being invalidated, we will require N matching subscribe calls to keep
    // them all active.
    let existing;
    Object.keys(self._subscriptions).some((id) => {
      const sub = self._subscriptions[id];
      if (sub.inactive
          && sub.name === name
          && EJSON.equals(sub.params, params)) {
        return existing = sub;
      }
    });

    //let id;
    if (existing) {
      id = existing.id;
      existing.inactive = false; // reactivate

      if (callbacks.onReady) {
        // If the sub is not already ready, replace any ready callback with the
        // one provided now. (It's not really clear what users would expect for
        // an onReady callback inside an autorun; the semantics we provide is
        // that at the time the sub first becomes ready, we call the last
        // onReady callback provided, if any.)
        // If the sub is already ready, run the ready callback right away.
        // It seems that users would expect an onReady callback inside an
        // autorun to trigger once the the sub first becomes ready and also
        // when re-subs happens.
        if (existing.ready) {
          callbacks.onReady();
        }
        else {
          existing.readyCallback = callbacks.onReady;
        }
      }

      // XXX COMPAT WITH 1.0.3.1 we used to have onError but now we call
      // onStop with an optional error argument
      if (callbacks.onError) {
        // Replace existing callback if any, so that errors aren't
        // double-reported.
        existing.errorCallback = callbacks.onError;
      }

      if (callbacks.onStop) {
        existing.stopCallback = callbacks.onStop;
      }
    }
    else {
      // New sub! Generate an id, save it locally, and send message.
      //id = Random.id();
      self._subscriptions[id] = {
        id,
        name,
        params: EJSON.clone(params),
        inactive: false,
        ready: false,
        readyDeps: new Tracker.Dependency(),
        readyCallback: callbacks.onReady,
        // XXX COMPAT WITH 1.0.3.1 #errorCallback
        errorCallback: callbacks.onError,
        stopCallback: callbacks.onStop,
        connection: self,
        remove() {
          delete this.connection._subscriptions[this.id];
          this.ready && this.readyDeps.changed();
        },
        stop() {
          this.connection._send({ msg: "unsub", id });
          this.remove();

          if (callbacks.onStop) {
            callbacks.onStop();
          }
        }
      };
      self._send({
        msg: "sub", id, name, params
      });
    }

    // return a handle to the application.
    const handle = {
      stop() {
        if (!hasOwn.call(self._subscriptions, id)) {
          return;
        }
        self._subscriptions[id].stop();
      },
      ready() {
        // return false if we've unsubscribed.
        if (!hasOwn.call(self._subscriptions, id)) {
          return false;
        }
        const record = self._subscriptions[id];
        record.readyDeps.depend();
        return record.ready;
      },
      subscriptionId: id
    };
    return handle;
  }

  getDownstream() {
    if (!this.downstream) {
      this.downstream = Meteor.default_server.sessions[this.downStreamConnection.id];
    }
    return this.downstream;
  }

  clearTimeout(id) {
    Meteor.clearTimeout(this.timeouts[id]);
    delete this.timeouts[id];
  }

  addDocument(collection, docId) {
    this.documents[`${collection}::${docId}`] = 1;
  }

  removeDocument(collection, docId) {
    delete this.documents[`${collection}::${docId}`];
  }

  onMessage(rawMessage) {
    if (this._heartbeat) {
      this._heartbeat.messageReceived();
    }
    const downstream = this.getDownstream();
    if (!downstream) {
      Meteor._debug("downstream connection doesn't exist any more, or it got all its results and released the upstream connection");
      this.service.tryToClose(this.downStreamConnection.id, true, false);
      return;
    }
    try {
      const msgTypeString = rawMessage.slice(0, 50); // NOTE: assumes the maximum length of a message type is about 40 chars, and that it will be at the beginning.
      const msgType = msgTypeString.split(/[,}]/)[0].split(":")[1].slice(1, -1);
      if (this.messagesToIgnore && this.messagesToIgnore.includes(msgType)) {
        // ignore
      }
      else if (msgType === "connected") {
        this.hasBeenConnected = true;
        this.options.onConnected();
      }
      else if (msgType === "ping") {
        this._send({ msg: "pong" });
      }
      else if (msgType === "result") {
        const msg = DDPCommon.parseDDP(rawMessage);
        if (this.methods[msg.id]) {
          this.methods[msg.id].return(msg.result);
          delete this.methods[msg.id];
          this.service.tryToClose(this.downStreamConnection.id);
        }
        this.clearTimeout(msg.id);
        this._livedata_result(msg);
      }
      else if (msgType === "update") {
        this._livedata_data(DDPCommon.parseDDP(rawMessage));
      }
      else if (this.simpleMessages.includes(msgType)) {
        if (msgType === "added") {
          const msg = DDPCommon.parseDDP(rawMessage);
          this.addDocument(msg.collection, msg.id);
        }
        if (msgType === "removed") {
          const msg = DDPCommon.parseDDP(rawMessage);
          this.removeDocument(msg.collection, msg.id);
        }
        if (msgType === "ready") {
          const msg = DDPCommon.parseDDP(rawMessage);
          msg.subs.forEach(subId => this.clearTimeout(subId));
        }
        else if (msgType === "nosub") {
          const msg = DDPCommon.parseDDP(rawMessage);
          this.clearTimeout(msg.id);
          Meteor.defer(() => {
            delete this.subscriptions[msg.id];
            this.service.tryToClose(this.downStreamConnection.id);
          });
        }
        this.getDownstream().socket.send(rawMessage);
      }
      else {
        //console.log(msg);
        //Meteor._debug("unexpected msg", `:${msgType}:`, { msg: msg.msg, id: msg.id });
      }
    }
    catch (e) {
      console.error(e);
      Meteor._debug("Exception while parsing DDP", e);
    }
  }
}
