import chai from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import PassthroughConnection from "../server/passthrough-connection.js";

const expect = chai.expect;
chai.use(sinonChai);


describe("passthrough-connection", () => {
  function getDownstream() {
    return {
      send: sinon.spy(),
      socket: {
        send: sinon.spy()
      }
    };
  }
  describe("lifecycle", () => {
    it("Should force close after 4 consecutive connection failures", () => {
      const service = {
        tryToClose: sinon.spy()
      };

      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.onDisconnect();
      con.onDisconnect();
      con.onDisconnect();
      con.onDisconnect();
      expect(service.tryToClose).to.have.been.calledWith("test", true, true);
      con.close();
    });

    it("Should remove all documents from downstream when upstream closes", () => {
      const service = {
        tryToClose: sinon.spy()
      };

      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.hasBeenConnected = true;
      con.documents = {
        "test::1": 1
      };
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onDisconnect();
      expect(downstream.send).to.have.been.calledWith({ msg: "removed", collection: "test", id: "1" });
    });

    it("Should send nosubs to downstream for each subscription when upstream closes", () => {
      const service = {
        tryToClose: sinon.spy()
      };

      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.hasBeenConnected = true;
      con.subscriptions = {
        1: 1
      };
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onDisconnect();
      expect(downstream.send).to.have.been.calledWith({ msg: "nosub", id: "1" });
    });

    it("Should close the upstream connection if a message is received and the downstream connection is not there", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.onMessage(``);
      expect(service.tryToClose).to.have.been.calledWith("test", true, false);
    });
  });

  describe("methods", () => {
    it("Should clear a timeout when a result message is received (for a valid method)", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.clearTimeout = sinon.spy();
      con.methods["1"] = {
        return: () => {}
      };
      con.getDownstream = getDownstream;
      con.onMessage(`{"msg":"result","id":"1"}`);
      expect(con.clearTimeout).to.have.been.calledWith("1");
    });

    it("Should clear a timeout when a result message is received (for an invalid method)", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.clearTimeout = sinon.spy();
      con.getDownstream = getDownstream;
      con.onMessage(`{"msg":"result","id":"1"}`);
      expect(con.clearTimeout).to.have.been.calledWith("1");
    });

    it("Should pass a result to the client", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      const resultSpy = sinon.spy();
      con.methods["1"] = {
        return: resultSpy
      };
      con.getDownstream = getDownstream;
      con.onMessage(`{"msg":"result","id":"1"}`);
      expect(resultSpy).to.have.been.called;
    });
  });

  describe("subscriptions", () => {
    it("should clear a timeout when a subscription becomes ready", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.clearTimeout = sinon.spy();
      con.subscriptions["1"] = "1";
      con.getDownstream = getDownstream;
      con.onMessage(`{"msg":"ready","subs":["1"]}`);
      expect(con.clearTimeout).to.have.been.calledWith("1");
    });

    it("should clear a timeout when a subscription errors", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.clearTimeout = sinon.spy();
      con.subscriptions["1"] = "1";
      con.getDownstream = getDownstream;
      con.onMessage(`{"msg":"nosub","id":"1"}`);
      expect(con.clearTimeout).to.have.been.calledWith("1");
    });

    it("should track added documents", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.subscriptions["1"] = "1";
      con.addDocument = sinon.spy();
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onMessage(`{"msg":"added","collection":"test","id":"1"}`);
      expect(con.addDocument).to.have.been.calledWith("test", "1");
    });

    it("should track removed documents", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.subscriptions["1"] = "1";
      con.removeDocument = sinon.spy();
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onMessage(`{"msg":"removed","collection":"test","id":"1"}`);
      expect(con.removeDocument).to.have.been.calledWith("test", "1");
    });


    it("should pass added messages to downstream", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.subscriptions["1"] = "1";
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onMessage(`{"msg":"added","id":"1"}`);
      expect(downstream.socket.send).to.have.been.calledWith(`{"msg":"added","id":"1"}`);
    });

    it("should pass removed messages to downstream", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.subscriptions["1"] = "1";
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onMessage(`{"msg":"removed","id":"1"}`);
      expect(downstream.socket.send).to.have.been.calledWith(`{"msg":"removed","id":"1"}`);
    });

    it("should pass ready messages to downstream", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.subscriptions["1"] = "1";
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onMessage(`{"msg":"ready","subs":["1"]}`);
      expect(downstream.socket.send).to.have.been.calledWith(`{"msg":"ready","subs":["1"]}`);
    });

    it("should pass nosub messages to downstream", () => {
      const service = {
        tryToClose: sinon.spy()
      };
      const con = new PassthroughConnection(service, { id: "test" }, "dummy", {});
      con.close();
      con.subscriptions["1"] = "1";
      const downstream = getDownstream();
      con.getDownstream = () => downstream;
      con.onMessage(`{"msg":"nosub","id":"1"}`);
      expect(downstream.socket.send).to.have.been.calledWith(`{"msg":"nosub","id":"1"}`);
    });
  });
});
