export { AWSHostNameDiscovery, EnvHostNameDiscovery, CmdHostNameDiscovery } from "./discovery/hostname-discovery.js";

export MongoServiceDiscovery from "./discovery/service-discovery.js";
export MicroService from "./service.js";
