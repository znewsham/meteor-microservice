import Future from "fibers/future";
import { Random } from "meteor/random";
import MongoServiceDiscovery from "./discovery/service-discovery.js";

const DefaultConnection = Meteor.isServer ? require("./server/passthrough-connection.js").default : require("./client/default-connection.js").default;

export default class MicroService {
  constructor(serviceName, options = {}) {
    this.serviceName = serviceName;
    this.ConnectionConstructor = options.connectionConstructor || DefaultConnection;
    this.serviceDiscovery = options.serviceDiscovery || new MongoServiceDiscovery(options.serviceDiscoveryOptions);
    this.upstreamConnections = {};
    this.loginMethod = options.loginMethod || (() => {});
  }

  _registerConsumerConnection(connection, userId) {
    if (!Meteor.isServer) {
      throw new Meteor.Error("Only avaialable on the server");
    }
    const service = this;
    if (!this.upstreamConnections[connection.id]) {
      const ddpAddress = this.serviceDiscovery.getDDPAddress(this.serviceName);
      if (!ddpAddress) {
        throw new Meteor.Error(400, `No healthy ${this.serviceName} services available`);
      }
      const upstream = new this.ConnectionConstructor(service, connection, ddpAddress, { loginMethod: this.loginMethod });
      upstream.userId = userId;
      this.upstreamConnections[connection.id] = upstream;
      try {
        upstream.loginMethod(userId);
      }
      catch (e) {
        this.tryToClose(connection.id, true, false);
        throw e;
      }
      connection.onClose(() => {
        if (this.upstreamConnections[connection.id]) {
          this.upstreamConnections[connection.id].hasBeenConnected = false;
          if (this.upstreamConnections[connection.id]) {
            _.values(this.upstreamConnections[connection.id].timeouts).forEach((timeout) => {
              clearTimeout(timeout);
            });
            this.tryToClose(connection.id, true, false);
          }
        }
      });
    }
    const con = this.upstreamConnections[connection.id];
    return con;
  }

  tryToClose(connectionId, force, tryToRecreate) {
    // NOTE: it seems we can't open and close connections without all the data being resent erroneously
    // return false;
    if (!this.upstreamConnections[connectionId]) {
      return true;
    }
    const canClose = force ;//|| Object.keys(this.upstreamConnections[connectionId].methods).length === 0 && Object.keys(this.upstreamConnections[connectionId].subscriptions).length === 0;
    if (canClose) {
      Object.values(this.upstreamConnections[connectionId].timeouts).forEach((timeout) => {
        clearTimeout(timeout);
      });
      this.upstreamConnections[connectionId].close();
      const oldUpstream = this.upstreamConnections[connectionId];
      delete this.upstreamConnections[connectionId];
      if (tryToRecreate) {
        const newUpstream = this._registerConsumerConnection(oldUpstream.downstream, oldUpstream.userId);
        newUpstream._subscriptions = oldUpstream._subscriptions;
      }
    }
    return canClose;
  }

  method(localName, remoteName, { beforeCall, timeout } = {}) {
    if (!Meteor.isServer) {
      throw new Meteor.Error("Only avaialable on the server");
    }
    const service = this;
    const methodObj = {};
    methodObj[localName] = function methodHandler(...args) {
      const methodContext = this;
      const methodId = methodContext.id || Random.id();
      if (!methodContext.connection) {
        // HACK: calls from the meteor server itself wont work without this.
        methodContext.connection = {

          id: "_self",
          onClose: () => {}
        };
      }
      const serviceInstance = service._registerConsumerConnection(methodContext.connection, methodContext.userId);
      const upstreamMsgId = serviceInstance._nextMethodId || 1;
      if (beforeCall) {
        const res = beforeCall.call(methodContext, ...args);
        if (_.isArray(res)) {
          args = res;
        }
      }
      const future = new Future();
      if (timeout) {
        serviceInstance.timeouts[upstreamMsgId] = Meteor.setTimeout(serviceInstance.onTimeout.bind(serviceInstance, "method", upstreamMsgId, methodId), timeout);
      }
      serviceInstance.call(
        remoteName,
        ...args,
        (err, res) => {
          if (err) {
            return future.throw(err);
          }
          return future.return(res);
        }
      );
      serviceInstance.methods[upstreamMsgId] = future;
      try {
        return future.wait();
      }
      catch (e) {
        throw e;
      }
      finally {
        if (serviceInstance.timeouts[upstreamMsgId]) {
          serviceInstance.clearTimeout(upstreamMsgId);
        }
        // NOTE: needs to be deferred - the onMessage event fires in a different event loop from this one returning.
        Meteor.defer(() => {
          delete serviceInstance.methods[upstreamMsgId];
          service.tryToClose(methodContext.connection.id);
        });
      }
    };
    Meteor.methods(methodObj);
  }

  publish(localName, remoteName, { beforeCall, timeout } = {}) {
    if (!Meteor.isServer) {
      throw new Meteor.Error("Only avaialable on the server");
    }
    const service = this;
    Meteor.publish(localName, function publishHandler(...args) {
      const publishContext = this;
      if (beforeCall) {
        const res = beforeCall.call(publishContext, ...args);
        if (_.isArray(res)) {
          args = res;
        }
      }
      const serviceInstance = service._registerConsumerConnection(publishContext.connection, publishContext.userId);

      if (!serviceInstance) {
        throw new Meteor.Error(500, "No registered connection");
      }
      const sub = serviceInstance.subscribe.apply(serviceInstance, [publishContext._subscriptionId, remoteName, ...args]);
      if (timeout) {
        serviceInstance.timeouts[publishContext._subscriptionId] = setTimeout(serviceInstance.onTimeout.bind(serviceInstance, "publish", sub.subscriptionId, publishContext._subscriptionId), timeout);
      }
      serviceInstance.subscriptions[sub.subscriptionId] = publishContext._subscriptionId;

      publishContext.onStop(() => {
        clearTimeout(serviceInstance.timeouts[publishContext._subscriptionId]);
        sub.stop();
      });
    });
  }
}


/*
MicroServer:{
  publish()
  call()
  apply()
  method()
  serviceDiscovery: MongoServiceDiscovery,
  connections:{
    downstreamConnectionId: PassthroughConnection
  }
}

PassthroughConnection: {
  downstream: Stream,
  documentIds: [{ collection: String, id: String }],
  _stream: Stream (upstream),
  subscriptions: {
    downstreamSubscriptionId: upstreamSubscriptionId
  }
}
*/
