import chai from "chai";
import sinon from "sinon";
import sinonChai from "sinon-chai";
import MicroService from "../service.js";
import DummyConnection from "./dummy-connection.js";
import DummyServiceDiscovery from "./dummy-discovery.js";

const expect = chai.expect;
chai.use(sinonChai);


describe("service", () => {
  describe("lifecycle", () => {
    it("Should use provided dummy ConnectionProvider", () => {
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection
      });
      expect(service.ConnectionConstructor).to.equal(DummyConnection);
    });

    it("Should use provided dummy DiscoveryService", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });
      expect(service.serviceDiscovery).to.equal(dummy);
    });

    it("Should create a connection on request", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });
      const con = {
        id: "test",
        onClose: () => {}
      };
      const upstream = service._registerConsumerConnection(con);
      expect(upstream).to.not.be.undefined;
    });

    it("Should register an onClose handler when registered", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });
      const con = {
        id: "test",
        onClose: sinon.spy()
      };
      const upstream = service._registerConsumerConnection(con);
      expect(con.onClose).to.have.been.called;
    });

    it("Should close an upstream connection when the downstream connection closes", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });
      const con = {
        id: "test",
        closeHandlers: [],
        onClose: (handler) => {
          con.closeHandlers.push(handler);
        }
      };
      const upstream = service._registerConsumerConnection(con);
      service.tryToClose = sinon.spy();
      con.closeHandlers[0]();
      expect(service.tryToClose).to.have.been.calledWith("test", true, false);
    });
  });

  describe("methods", () => {
    it("Should create a method handler when called", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });

      service.method("localName", "remoteName");
      expect(Meteor.default_server.method_handlers.localName).to.not.be.undefined;
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should call a beforeCall handler", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = sinon.spy();
      service.method("localName", "remoteName", { beforeCall });
      Meteor.call("localName");
      expect(beforeCall).to.have.been.called;
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should respect the throw of a beforeCall handler", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => {
        throw new Meteor.Error(100, "");
      };
      service.method("localName", "remoteName", { beforeCall });
      function doit() {
        Meteor.call("localName");
      }
      expect(doit).to.throw(Meteor.Error);
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should respect the result of a beforeCall handler", () => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        call(name, ...args) {
          expect(args.length).to.equal(4);
          expect(args[0]).to.equal(1);
          expect(args[1]).to.equal(2);
          expect(args[2]).to.equal(3);
          args[3](null, null);
        };
      };

      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => [1, 2, 3];
      service.method("localName", "remoteName", { beforeCall });
      Meteor.call("localName");
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should pass an error down the chain", (done) => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        call(name, ...args) {
          args[3](new Meteor.Error(100, "test"), null);
        };
      };

      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => [1, 2, 3];
      service.method("localName", "remoteName", { beforeCall });
      Meteor.call("localName", (err, res) => {
        expect(err).to.not.be.undefined;
        done();
      });
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should pass a result down the chain", (done) => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        call(name, ...args) {
          args[3](undefined, 6);
        };
      };

      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => [1, 2, 3];
      service.method("localName", "remoteName", { beforeCall });
      Meteor.call("localName", (err, res) => {
        expect(err).to.be.undefined;
        expect(res).to.equal(6);
        done();
      });
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should return an error if the upstream times out", (done) => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        call(name, ...args) {
          this._nextMethodId = 2;
        };
      };

      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => [1, 2, 3];
      service.method("localName", "remoteName", { beforeCall, timeout: 300 });
      Meteor.call("localName", (err, res) => {
        expect(err).to.not.be.undefined;
        done();
      });
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should not timeout if an error is received", (done) => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        call(name, ...args) {
          args[3](new Meteor.Error(100, "test"), null);
        }

        onTimeout() {
        }
      };

      ExtendedConnection.prototype.onTimeout = sinon.spy();

      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => [1, 2, 3];
      service.method("localName", "remoteName", { beforeCall, timeout: 10 });
      Meteor.call("localName", (err, res) => {
        expect(err).to.not.be.undefined;
      });
      setTimeout(() => {
        expect(ExtendedConnection.prototype.onTimeout).to.not.have.been.called;
        done();
      }, 200);
      delete Meteor.default_server.method_handlers.localName;
    });

    it("Should not timeout if a result is received", (done) => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        call(name, ...args) {
          args[3](undefined, 6);
        }

        onTimeout() {
        }
      };

      ExtendedConnection.prototype.onTimeout = sinon.spy();

      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => [1, 2, 3];
      service.method("localName", "remoteName", { beforeCall, timeout: 10 });
      Meteor.call("localName", (err, res) => {
        expect(res).to.equal(6);
      });
      setTimeout(() => {
        expect(ExtendedConnection.prototype.onTimeout).to.not.have.been.called;
        done();
      }, 200);
      delete Meteor.default_server.method_handlers.localName;
    });
  });

  describe.only("publications", () => {
    it("Should create a publication handler when called", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });

      service.publish("localName", "remoteName");
      expect(Meteor.default_server.publish_handlers.localName).to.not.be.undefined;
      delete Meteor.default_server.publish_handlers.localName;
    });

    it("Should call a beforeCall handler", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = sinon.spy();
      service.publish("localName", "remoteName", { beforeCall });
      Meteor.default_server.publish_handlers.localName.call({
        onStop() {

        },
        connection: {
          id: "test",
          onClose() {

          }
        }
      });
      expect(beforeCall).to.have.been.called;
      delete Meteor.default_server.publish_handlers.localName;
    });

    it("Should respect the throw of a beforeCall handler", () => {
      const dummy = new DummyServiceDiscovery();
      const service = new MicroService("test", {
        connectionConstructor: DummyConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => {
        throw new Meteor.Error(100, "");
      };
      service.publish("localName", "remoteName", { beforeCall });
      function doit() {
        Meteor.default_server.publish_handlers.localName.call({
          onStop() {

          },
          connection: {
            id: "test",
            onClose() {

            }
          }
        });
      }
      expect(doit).to.throw(Meteor.Error);
      delete Meteor.default_server.publish_handlers.localName;
    });

    it("Should respect the result of a beforeCall handler", () => {
      const dummy = new DummyServiceDiscovery();
      class ExtendedConnection extends DummyConnection {
        subscribe(name, ...args) {
          expect(args.length).to.equal(4);
          expect(args[1]).to.equal(1);
          expect(args[2]).to.equal(2);
          expect(args[3]).to.equal(3);
          return {
            subscriptionId: args[0]
          };
        };
      };
      const service = new MicroService("test", {
        connectionConstructor: ExtendedConnection,
        serviceDiscovery: dummy
      });

      const beforeCall = () => {
        return [1, 2, 3];
      };
      service.publish("localName", "remoteName", { beforeCall });
      Meteor.default_server.publish_handlers.localName.call({
        onStop() {

        },
        connection: {
          id: "test",
          onClose() {

          }
        }
      });
      delete Meteor.default_server.publish_handlers.localName;
    });
  });
});
