Package.describe({
  name: "znewsham:microservice",
  version: "0.0.1",
  // Brief, one-line summary of the package.
  summary: "",
  // URL to the Git repository containing the source code for this package.
  git: "",
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: "README.md"
});

Package.onUse((api) => {
  api.versionsFrom("1.8");
  api.use("ecmascript");
  api.use("ejson");
  api.use("ddp-client");
  api.use("underscore");
  api.mainModule("server.js", "server");
});

Package.onTest((api) => {
  api.use("ecmascript");
  Npm.depends({
    chai: "4.2.0",
    sinon: "7.1.1",
    "sinon-chai": "3.2.0"
  });
  api.use("ejson");
  api.use("underscore");
  api.use("meteor");
  api.use("znewsham:microservice");
  api.use("meteortesting:mocha");
  api.addFiles("./tests/passthrough-connection.js");
  api.addFiles("./tests/service.js");
  api.addFiles("./tests/dummy-connection.js");
  api.addFiles("./tests/dummy-discovery.js");
  api.mainModule("tests/microservice-tests.js", "server");
});
