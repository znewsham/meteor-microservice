import { EnvHostNameDiscovery } from "./hostname-discovery.js";

export default class MongoServiceDiscovery {
  static _connections = [];

  constructor(options = {}) {
    if (!options.hostNameDiscovery) {
      this.hostNameDiscovery = new EnvHostNameDiscovery();
    }
    else {
      this.hostNameDiscovery = options.hostNameDiscovery;
    }
    if (options.collection) {
      this.collection = options.collection;
    }
    this.latestHealthCheckMS = options.latestHealthCheckMS || 30000;
    if (!this.collection) {
      this.collection = MongoServiceDiscovery.GetCollection(options.collectionName, options.connection);
    }
  }

  static GetCollection(collectionName = "__serviceDiscovery", connection = undefined) {
    let conObj = _.findWhere(MongoServiceDiscovery._connections, { collectionName, connection });
    if (!conObj) {
      conObj = new Meteor.Collection(collectionName, { connection });
      MongoServiceDiscovery._connections.push(conObj);
    }
    return conObj;
  }

  async register(name, protocol = "ws", host = undefined, port = process.env.PORT, options = {}) {
    if (host === undefined) {
      host = await this.hostNameDiscovery.getHost();
    }
    if (!options.healthCheckFunction) {
      options.healthCheckFunction = () => {
        this.collection.update(
          { name, host, port },
          {
            $setOnInsert: { name, host, port },
            $set: { lastHealthyTime: new Date(), protocol }
          },
          { upsert: true }
        );
      };
    }
    options.healthCheckFunction.call(this);
    if (options.healthCheckIntervalMS === undefined) {
      options.healthCheckIntervalMS = 10000;
    }
    if (options.healthCheckIntervalMS) {
      this.healthCheckInterval = Meteor.setInterval(options.healthCheckFunction.bind(this), options.healthCheckIntervalMS);
    }
  }

  getDDPAddress(name, options = {}) {
    const query = _.extend(
      { name },
      options.query || {},
      { lastHealthyTime: { $gt: new Date(new Date().getTime() - this.latestHealthCheckMS) } }
    );

    const servers = this.collection.find(
      query,
      {
        fields: {
          host: true,
          port: true,
          protocol: true,
          path: true
        }
      }
    ).fetch();
    const server = _.shuffle(servers)[0];

    if (!server) {
      throw new Meteor.Error(400, `No healthy ${name} services available`);
    }
    return `${server.host}:${server.port}/${server.path || ""}`;
  }
}
