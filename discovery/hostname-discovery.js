import child_process from "child_process";
const spawn = child_process.spawn;

export class EnvHostNameDiscovery {
  constructor() {
    this.envVar = "HOSTNAME";
  }

  getHost() {
    return Promise.resolve(process.env[this.envVar]);
  }
}

export class CmdHostNameDiscovery {
  constructor(cmd, args) {
    this.cmd = cmd;
    this.args = args;
    this.hostName = false;
  }

  getHost() {
    const self = this;
    if (this.hostName) {
      return Promise.resolve(this.hostName);
    }
    return new Promise((resolve, reject) => {
      const child = spawn(this.cmd, this.args, {});
      child.stdout.on("data", (msg) => {
        self.hostName = `${msg}`;
      });
      child.on("close", () => {
        resolve(self.hostName);
      })
      .on("error", (err) => {
        reject(err);
      });
    });
  }
}

export class AWSHostNameDiscovery extends CmdHostNameDiscovery {
  constructor() {
    super("curl", ["http://169.254.169.254/latest/meta-data/local-hostname"]);
  }
}
