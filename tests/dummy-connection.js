import PassthroughConnection from "../server/passthrough-connection.js";

export default class DummyConnection {
  static DEFAULT_MESSAGES_TO_IGNORE = ["pong"];

  static DEFAULT_SIMPLE_MESSAGES = ["added", "changed", "removed", "nosub", "ready"];

  static DEFAULT_LOGIN_TOKEN_TIMEOUT = 90 * 24 * 60 * 60 * 1000;

  constructor(service, connection, ddpAddress, options = {}) {
    this.downStreamConnection = connection;
    this.subscriptions = {};
    this.methods = {};
    this.timeouts = {};
    this.documents = {};
    this.service = service;
    this.requireLogin = options.requireLogin || false;
    this.loginMethod = options.loginMethod || (() => {});
    this.hasBeenConnected = false;
    this.loginTokenTimeout = DummyConnection.DEFAULT_LOGIN_TOKEN_TIMEOUT;
    this.messagesToIgnore = DummyConnection.DEFAULT_MESSAGES_TO_IGNORE;
    this.simpleMessages = DummyConnection.DEFAULT_SIMPLE_MESSAGES;
    this.retryCount = 0;
  }

  call(name, ...args) {
    let callback;
    if (_.isFunction(args[args.length - 1])) {
      callback = args.splice(args.length - 1, 1)[0];
    }
    callback(null, null);
  }

  subscribe(...args) {
    return {
      subscriptionId: "test"
    };
  }

  clearTimeout(id) {
    Meteor.clearTimeout(this.timeouts[id]);
  }

  getDownstream() {
    return {
      id: "test"
    };
  }

  onTimeout(...args) {
    return PassthroughConnection.prototype.onTimeout.call(this, ...args);
  }
}
